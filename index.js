//Dependencies
const express = require('express');
const app = express();
const http = require("http").Server(app);
const config = require("./config/config.json");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

app.use(express.json());

process.on("SIGINT", function () {
  mongoose.disconnect(function () {
    console.log("Mongoose disconnected on app termination");
    process.exit(0);
  });
});

(async () => {
  require("./routes/route.js")(app);
  require("./controllers/queueWorker.js")();

  http.listen(config.port, function () {
    console.log("listening on *:", config.port);
  });
})();
