const moment = require("moment");
const config = require("../config/config.json");
const Logger = require("../helpers/logger.js");
const Bull = require("bull");
const User = require('../models/user.js');
const emailQueue = new Bull("emailQueue", {
    redis: config.redis
});


module.exports = {
    initQueue
};

async function addEmailQueue (data){
    const date = moment(data.birthday);
    const day = date.date();
    const month = date.month() + 1;

    const job = await emailQueue.add(data, 
        { 
            jobId: data._id.toString(),
            attempts: 3,
            backoff: (3600000 * 3),
            removeOnComplete: true,
            removeOnFail: false,
            repeat: {
                cron: `00 09 ${day} ${month} *`,
                tz: data.location,
            },
        });

    if (await job.isFailed()) await job.retry();
};

async function initQueue(data) {
    try {
        let pushFlag = true;
        let jobs = await emailQueue.getJobs();

        if (!jobs.length || jobs.length === 0) {
            await addEmailQueue(data);
        } else {
            for (let job of jobs) {
                if (job && job.data && data.id === job.data.id) pushFlag = false;
            }
            if (pushFlag) {
                await addEmailQueue(data);
            }
        }
        return;
    } catch (err) {
        Logger.err(e);
    }
}