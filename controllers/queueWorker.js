module.exports = function() {
    const moment = require("moment");
    const Bull = require("bull");
    let axios = require("axios");
    const Logger = require("../helpers/logger.js");
    const config = require("../config/config.json");
    const emailQueue = new Bull("emailQueue", {
        redis: config.redis,
    });

    function generateGreeting(type, firstName, lastName) {
      switch (type.toLowerCase()) {
        case 'birthday':
          return `Selamat ulang tahun, ${firstName} ${lastName}!`;
        case 'anniv':
          return `Selamat hari jadi, ${firstName} ${lastName}!`;
        case 'new year':
          return `Selamat Tahun Baru, ${firstName} ${lastName}!`;
        default:
          return 'Tipe ucapan tidak valid.';
      }
    }

    emailQueue.process(async function(job, callback) {
        try {
            Logger.info(`start processing ${job.data.id} schedule: ${job.data.birthday}`);
              try {
                const response = await axios.post('https://email-service.digitalenvision.com.au/send-email', 
                  {
                    "email": "lanegah@gmail.com",
                    "message": generateGreeting(job.data.type, job.data.firstName, job.data.lastName),
                  }
                );

                console.log(`Email sent successfully to lanegah@gmail.com. Response:`, response.data);

                return callback(null, job.data); // Mengembalikan status HTTP
              } catch (error) {
                const axiosError = error;

                if (axiosError.response) {
                  // Respons dari server dengan status error
                  console.error(`Error response from server:`, axiosError.response.data);
                  return callback(null, null);
                } else if (axiosError.request) {
                  // Tidak mendapatkan respons dari server
                  console.error(`No response received from the server`);
                  return callback(null, null); // Internal Server Error
                } else {
                  // Kesalahan lainnya
                  console.error('Error:', axiosError.message);
                  return callback(null, null); // Internal Server Error
                }
              }
            return callback(null, job.data);
        } catch (e) {
            Logger.err(e)
            return callback(null, null);
        }
    });
    emailQueue.on("completed", function(id, data) {
        Logger.info("Finish job");
        emailQueue.clean(0);
    });

};
