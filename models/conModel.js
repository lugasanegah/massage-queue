var mongoose = require('mongoose');
var config = require('../config/config.json');

mongoose.connect(config.mongoConnection, { 
    promiseLibrary: global.Promise,
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.set('useCreateIndex', true);

module.exports = mongoose;