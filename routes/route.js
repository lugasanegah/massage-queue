//Profile
const User = require('../models/user.js');
const schedulerControl = require('../controllers/scheduleController.js');

module.exports = function (app) {
  
  app.post('/user', async (req, res) => {
  try {
    const { firstName, lastName, birthday, location, email, type} = req.body;
    const user = new User({ firstName, lastName, birthday, location, email, type });
    await user.save();
    const data = await schedulerControl.initQueue(user);
    return res.status(201).json(user);
  } catch (error) {
    console.error(error);
    return res.status(500).send('Internal Server Error');
  }
  });

  app.put('/user/:id', async (req, res) => {
    try {
      const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
      return res.status(200).json(user);
    } catch (error) {
      console.error(error);
      return res.status(500).send('Internal Server Error');
    }
  });

  app.delete('/user/:id', async (req, res) => {
    try {
      await User.findByIdAndDelete(req.params.id);
      return res.sendStatus(204);
    } catch (error) {
      console.error(error);
      return res.status(500).send('Internal Server Error');
    }
  });
};
