var mongoose = require('./conModel.js');

var countrySchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  birthday: Date,
  location: String,
  type: String
},
{
    timestamps: true
});

var User = mongoose.model('User', countrySchema);

module.exports = User;