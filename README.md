# massage-queue

# Greeting Scheduler

This project is created to send greetings to users who are input into the system. It will send greetings every day at 9 AM based on the user's date of birth and their local timezone. Below is an example API input:

## Example API Input

### Endpoint: /user
### Method: POST
### Body:
```json
{
  "firstName": "Titan",
  "lastName": "Iddi",
  "birthday": "1992-02-25",
  "location": "Asia/Jakarta",
  "type": "Birthday"
}
```
# Instal dependencies
```
npm install
```

# How to Start 
```
npm start
```
